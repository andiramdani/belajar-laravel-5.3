<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/', function () {
//     return view('simple');
// });

// Route::get('/', function () {
//     return view('/blog/index');
// });

// Route::get('/about', function () {
//     return view('/blog/about');
// });

// Route::get('/','BlogController@index');

// Route::get('/about', function () {
//     return view('/blog/about');
// });

// // tambah data
// Route::get('/create','BlogController@create');
// Route::post('/blog','BlogController@store');

// // tampilkan blog
// Route::get('/{id}','BlogController@show');

// // edit blog
// Route::get('/{id}/edit','BlogController@edit');
// Route::put('/{id}','BlogController@update');

// // hapus blog
// Route::delete('/{id}','BlogController@destroy');

// verifikasi token
Route::get('/verify/{token}/{id}', 'Auth\RegisterController@verify_register');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
