@extends('layouts.default')
@section('title', 'singleblog')
@section('content')
    <h1 class="text-center">SELAMAT DATANG DI HALAMAN SINGLE BLOG</h1>
    <hr>

    <h1> {{ $blogs->title }} </h1>
    <p> {{ $blogs->description }} </p>
    <hr>
    <a href="/"> Back Home </a>
@endsection
