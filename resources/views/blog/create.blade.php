@extends('layouts.default')
@section('title', 'singleblog')
@section('content')
    <h1 class="text-center">TAMBAH BLOG</h1>
    <hr>

  <!--   @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif -->
    

    <form action="/blog" method="post">
    	<input type="text" name="title" value="{{ old('title') }}"><br>
            @if ($errors->has('title'))
                <div class="alert alert-danger">
                    {{ $errors->first('title') }}
                </div>
            @endif

    	<textarea name="description" rows="8" cols="40">{{ old('description') }}</textarea>
            @if ($errors->has('description'))
                <div class="alert alert-danger">
                    {{ $errors->first('description') }}
                </div>
            @endif
    	<input type="submit" name="submit" value="SAVE">
    	{{ csrf_field() }}
    </form>
    <hr>
    <a href="/"> Back Home </a>
@endsection
