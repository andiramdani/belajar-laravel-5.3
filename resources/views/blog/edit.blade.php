@extends('layouts.default')
@section('title', 'singleblog')
@section('content')
    <h1 class="text-center">EDIT BLOG INI</h1>
    <hr>

    <form action="/{{$blog->id}}" method="post">
    	<input type="text" name="title" value="{{ $blog->title }}"><br>
    	<textarea name="description" rows="8" cols="40">{{ $blog->description }}</textarea>
    	<input type="submit" name="submit" value="edit">
    	<input type="hidden" name="_method" value="PUT">
    	<!-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->
    	{{ csrf_field() }}
    </form>
    <hr>
    <a href="/"> Back Home </a>
@endsection
