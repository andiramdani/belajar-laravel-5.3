<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\DB;

use App\Http\Requests\ErrorFormRequest;

use DB;
use App\Models\blog;

class BlogController extends Controller
{
    public function index() {

        // dd($blogs);

        // use elequint insert biasa
        // $blog = new Blog;
        // $blog->title = 'halo cimahi';
        // $blog->description = 'isi dari halo cimahi';
        // $blog->save();

        // use elequint insert mass assigment

        // Blog::create([
        //     'title' => 'hallo bekasi',
        //     'description' => 'isi dari hallo bekasi',
        //     'created_at' => '2018-09-18 04:45:05',
        //     'updated_at' => '2018-09-18 04:45:05',
        // ]);

        // use elequint update biasa
        // $blog =  Blog::where('title', 'halo cimahi')->where('created_at', '2018-09-18 04:55:05')->first();
        // $blog->title = 'hallo majalengka';
        // $blog->save();


         // use elequint update mass assigment
        // $blog =  Blog::where('title', 'halo cimahi')->where('created_at', '2018-09-18 04:55:05')->first();
        // $blog->title = 'hallo majalengka';
        // $blog->save();

        // Blog::find(1)->update(['description' => 'ini dari halo majalengka']);
        // Blog::where('title', 'hallo majalengka')->update(['description' => 'ini dari halo majalengka']);

        // use elequint delete biasa
        // $blog =  Blog::find(7);
        // $blog->delete();

        // use elequint delete ke-2
        // Blog::destroy(8,10) 

        // dd($blog);

        // restore data softdelete
        // Blog::withTrashed()->where('title', 'halo cimahi 55')->restore();

        $blogs = blog::all();

        // show data with trashed / yang terhapus softdelete
        // $blogs = blog::withTrashed()->get();

		return view('blog/index', ['blogs' => $blogs] );
    }

    public function create() {

        return view('blog/create');
    }

    public function store(ErrorFormRequest $request) {

        // $this->validate($request, [
        //     'title' => 'required|unique:Blogs|min:5',
        //     'description' => 'required:min:10:max:15',
        // ]);

        $blog               = new Blog;
        $blog->title        = $request->title;
        $blog->description  = $request->description;
        $blog->save();

        return redirect('/');
    }

    public function show($id) {
        
        
        	// $nilai = 'ini adalah linknya '. $id;
        	// $user = 'Andi Ramdani';

        	// sample over data array
        	// $users = ['andi', 'gilang', 'rivan']; 

            // cara Query Bulider for insert
            // DB::table('users')->insert([
            //     ['username' => 'andiramdani', 'password'=> '12345']
            // ]);

            // cara Query Bulider for update
            // DB::table('users')->where('username', 'andi')
            //                   ->update(['username' => 'alexander']);

            // cara Query Bulider for delete
            // DB::table('users')->where('username', 'andiramdani')->delete();

            // $users = DB::table('users')->get();

            // $unescaped = '<script> alert("hello"); </script>';
            // $unescaped2 = '<b> alert("hello"); </b>';
        	
    		// return view('blog/single', ['blog' => $nilai, 'users' => $users, 'unescaped' => $unescaped, 'unescaped2' => $unescaped2]);

            // return view('blog/single', ['blog' => $nilai, 'users' => $users]);

            // use kolom blogs
        

        $blogs = blog::find($id);

        if (!$blogs) {
            abort(404);
        }

        // dd($blogs);
        return view('blog/single', ['blogs' => $blogs]);
    }

    public function edit($id) {

        $blog = Blog::find($id);

        if (!$blog) {
            abort(404);
        }

        return view('blog/edit', ['blog' => $blog]);
    }

    public function update(Request $request, $id) {

        // dd($request);

        $blog =  Blog::find($id);
        $blog->title        = $request->title;
        $blog->description  = $request->description;
        $blog->save();

        return redirect('/' . $id);
    }

    public function destroy($id) {

        $blog =  Blog::find($id);
        $blog->delete();
        return redirect('/');
    }
}
