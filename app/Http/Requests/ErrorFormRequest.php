<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ErrorFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return True;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:5',
            'description' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Judul Tidak Boleh Kosong',
            'title.min' => 'Judul Minimal 5 Karakter',
            'description.required' => 'deskripsi tidak boleh kosong'
        ];
    }
}
