<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class blog extends Model
{	
	// use SoftDeletes;

	// protected $dates = ['deleted_at'];

	// disable updated and created kolom
    // public $timestamps = false;

    // custome untuk nama kolom yang berbeda
    // const CREATED_AT = 'creation_date';
    // const UPDATED_AT = 'last_update'; 

	// whitelist
    protected $fillable = ['title','description'];

    // Blacklist
    // protected $guarded = ['created_at'];
}
