<?php

use Illuminate\Database\Seeder;

class BlogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for ($i=0; $i < 10 ; $i++) { 
    		DB::table('blogs')->insert([
            	'title' => 'hallo bandung',
            	'description' => 'ini konten dari hallo bandung',
        	]);	
    	}
    }
}
